import numpy as np
import easynn as nn

# Create a numpy array of 10 rows and 5 columns.
# Set the element at row i and column j to be i+j.
def Q1():
    arr = np.zeros((10, 5))
    row, col = arr.shape
    for i in  range(0, row):    
        for j in range(0, col):
            arr[(i, j)] = i+j
    return arr

# Add two numpy arrays together.
def Q2(a, b):
    added_array = np.add(a, b)
    return added_array

# Multiply two 2D numpy arrays using matrix multiplication.
def Q3(a, b):
    ans = a.dot(b)
    return ans

# For each row of a 2D numpy array, find the column index
# with the maximum element. Return all these column indices.
def Q4(a):
    row, col = a.shape
    for i in range(0, row):
        max_element = max(a[i-1])
        result = np.where(a[i-1] == max_element)

    return result[0][0]

# Solve Ax = b.
def Q5(A, b):
    x = np.linalg.solve(A, b)
    return x

# Return an EasyNN expression for a+b.
def Q6():
    return None

# Return an EasyNN expression for a+b*c.
def Q7():
    return None

# Given A and b, return an EasyNN expression for Ax+b.
def Q8(A, b):
    return None

# Given n, return an EasyNN expression for x**n.
def Q9(n):
    return None

# Return an EasyNN expression to compute
# the element-wise absolute value |x|.
def Q10():
    return None

if __name__ == "__main__":
    Q2(np.array([1,2,3,4,5,6,7,8,9,10]), np.array([10,9,8,7,8,6,5,4,3,2,1]))
