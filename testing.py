import numpy as np

def grade_Q3():
    a = np.random.rand(100, 10)
    row, col = a.shape
    max_indices = []
    for i in range(0, row):
        print(i)
        print(a[i])
        max_element = max(a[i])
        print(max_element)
        result = np.where(a[i] == max_element)
        max_indices.append(result[0][0])
    
    print(len(max_indices))

if __name__ == "__main__":
    grade_Q3()